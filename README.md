# Contao-Block-Default

## About / Function

This extension for Contao Open Source CMS extended all content elements with the default TailwindCSS class `mb-12`. It is customized to work together with [Conplate-Starterkit](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit).
